package com.nillis.edsi.parse;

import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;

import static org.junit.jupiter.api.Assertions.*;

class SExpressionTest {
  @Test
  void constructEmptyList() {
    Deque<String> empty = new ArrayDeque<>();
    empty.add("(");
    empty.add(")");
    SExpression elem = new SExpression(empty);
    assertEquals(empty.size(), 0);
    assertNull(elem.getFirst());
    assertNull(elem.getRest());
  }

  @Test
  void constructSimpleList() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add(")");
    SExpression elem = new SExpression(list);
    assertTrue(elem.getFirst() instanceof Atom);
    assertEquals(elem.getFirst().toString(), "a");
    assertNull(elem.getRest());
  }

  @Test
  void constructDualList() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add("b");
    list.add(")");
    SExpression elem = new SExpression(list);
    assertTrue(elem.getFirst() instanceof Atom);
    assertEquals(elem.getFirst().toString(), "a");
    assertTrue(elem.getRest() instanceof SExpression);
    SExpression rest = (SExpression)elem.getRest();
    assertTrue(rest.getFirst() instanceof Atom);
    assertEquals(rest.getFirst().toString(), "b");
    assertNull(rest.getRest());
  }

  @Test
  void constructListWithQuotedElement() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add("'");
    list.add("b");
    list.add("c");
    list.add(")");
    SExpression elem = new SExpression(list);
    assertEquals(list.size(), 0);
    assertTrue(elem.getFirst() instanceof Atom);
    assertEquals(elem.getFirst().toString(), "a");
    assertTrue(elem.getRest() instanceof SExpression);
    SExpression rest = (SExpression)elem.getRest();
    assertTrue(rest.getFirst() instanceof SExpression);
    SExpression quote = (SExpression)rest.getFirst();
    assertEquals(quote.getFirst().toString(), "'");
    assertEquals(quote.getRest().toString(), "b");
    assertEquals(((SExpression)rest.getRest()).getFirst().toString(), "c");
    assertNull(((SExpression)rest.getRest()).getRest());
  }

  @Test
  void constructListWithQuotedListElement() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add("'");
    list.add("(");
    list.add("b");
    list.add(")");
    list.add("c");
    list.add(")");
    SExpression elem = new SExpression(list);
    assertEquals(list.size(), 0);
    assertTrue(elem.getFirst() instanceof Atom);
    assertEquals(elem.getFirst().toString(), "a");
    assertTrue(elem.getRest() instanceof SExpression);
    SExpression rest = (SExpression)elem.getRest();
    assertTrue(rest.getFirst() instanceof SExpression);
    SExpression quote = (SExpression)rest.getFirst();
    assertEquals(quote.getFirst().toString(), "'");
    assertTrue(quote.getRest() instanceof SExpression);
    SExpression blist = (SExpression)quote.getRest();
    assertEquals(blist.getFirst().toString(), "b");
    assertNull(blist.getRest());
    assertEquals(((SExpression)rest.getRest()).getFirst().toString(), "c");
    assertNull(((SExpression)rest.getRest()).getRest());
  }

  @Test
  void appendWorksOnEmptyLists() {
    SExpression empty = new SExpression(null, null);
    Element atom = new Atom("a");
    empty.append(atom);
    assertEquals(empty.getFirst(), atom);
    assertNull(empty.getRest());
  }

  @Test
  void appendWorksOnSingletonLists() {
    SExpression single = new SExpression(new Atom("a"), null);
    single.append(new Atom("b"));
    assertEquals(single.toString(), "(a b)");
  }

  @Test
  void appendThrowsOnPair() {
    SExpression empty = new SExpression(new Atom("a"), new Atom("b"));
    assertThrows(IllegalArgumentException.class, () -> empty.append(new Atom("c")));
  }

  @Test
  void lengthOfEmptyList() {
    assertEquals(SExpression.EMPTY_LIST.length(), 0);
  }

  @Test
  void lengthOfSingletonList() {
    assertEquals(new SExpression(new Atom("a"), null).length(), 1);
  }

  @Test
  void lengthOfPair() {
    assertEquals(new SExpression(new Atom("a"), new Atom("b")).length(), -1);
  }

  @Test
  void lengthOfSimpleList() {
    assertEquals(new SExpression(new Atom("a"), new SExpression(new Atom("b"), null)).length(), 2);
  }

  @Test
  void toStringEmptyList() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add(")");
    SExpression elem = new SExpression(list);
    assertEquals(elem.toString(), "()");
  }

  @Test
  void toStringSimpleList() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add(")");
    SExpression elem = new SExpression(list);
    assertEquals(elem.toString(), "(a)");
  }

  @Test
  void toStringDualList() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add("b");
    list.add(")");
    SExpression elem = new SExpression(list);
    assertEquals(elem.toString(), "(a b)");
  }

  @Test
  void constructPair() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add(".");
    list.add("b");
    list.add(")");
    SExpression elem = new SExpression(list);
    assertTrue(elem.getFirst() instanceof Atom);
    assertEquals(elem.getFirst().toString(), "a");
    assertTrue(elem.getRest() instanceof Atom);
    assertEquals(elem.getRest().toString(), "b");
  }

  @Test
  void toStringPair() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add(".");
    list.add("b");
    list.add(")");
    SExpression elem = new SExpression(list);
    assertEquals(elem.toString(), "(a . b)");
  }

  @Test
  void shallowCopyWorksOnSimpleList() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add("b");
    list.add(")");
    SExpression main = new SExpression(list);
    SExpression copy = main.shallowCopy();
    assertTrue(main != copy);
    assertEquals(copy.toString(), main.toString());
  }

  @Test
  void shallowCopyWorksOnComplexList() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add("b");
    list.add(")");
    SExpression inner = new SExpression(list);
    SExpression main = new SExpression(new Atom("c"), new SExpression(inner, null));
    SExpression copy = main.shallowCopy();
    assertEquals(copy.toString(), main.toString());
    assertTrue(main.getFirst() == copy.getFirst());
    assertTrue(main.getRest() != copy.getRest());
  }

  @Test
  void lastPairNullList() {
    assertTrue(SExpression.EMPTY_LIST.lastPair() == SExpression.EMPTY_LIST);
  }

  @Test
  void lastPairSingletonList() {
    SExpression singleton = new SExpression(new Atom("a"), null);
    assertTrue(singleton.lastPair() == singleton);
  }

  @Test
  void lastPairSimpleList() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add("b");
    list.add(")");
    SExpression inner = new SExpression(list);
    SExpression lastPair = inner.lastPair();
    assertEquals(lastPair.toString(), "(b)");
  }

  @Test
  void lastPairListEndedWithPair() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add("b");
    list.add("c");
    list.add(".");
    list.add("d");
    list.add(")");
    SExpression inner = new SExpression(list);
    SExpression lastPair = inner.lastPair();
    assertEquals(lastPair.toString(), "(c . d)");
  }
}