package com.nillis.edsi.parse;

import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Deque;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ElementTest {
  @Test
  void parseAtom() {
    Deque<String> atom = new ArrayDeque<>();
    atom.add("a");
    Element elem = Element.parse(atom);
    assertEquals(atom.size(), 0);
    assertTrue(elem instanceof Atom);
    assertEquals(elem.toString(), "a");
  }

  @Test
  void parseNumericAtom() {
    Deque<String> atom = new ArrayDeque<>();
    atom.add("10");
    Element elem = Element.parse(atom);
    assertEquals(atom.size(), 0);
    assertTrue(elem instanceof NumericAtom);
    assertEquals(elem.toString(), "10");
  }

  @Test
  void parseStringAtom() {
    String token = "\"test\"";
    Deque<String> atom = new ArrayDeque<>();
    atom.add(token);
    Element elem = Element.parse(atom);
    assertEquals(atom.size(), 0);
    assertTrue(elem instanceof StringAtom);
    assertEquals(elem.toString(), token);
  }

  @Test
  void parseQuotedAtom() {
    Deque<String> quoted = new ArrayDeque<>();
    quoted.add("'");
    quoted.add("a");
    Element elem = Element.parse(quoted);
    assertEquals(quoted.size(), 0);
    assertTrue(elem instanceof SExpression);
    assertEquals(((SExpression)elem).getFirst().toString(), "'");
    assertEquals(((SExpression)elem).getRest().toString(), "a");
  }

  @Test
  void parseQuotedList() {
    Deque<String> quoted = new ArrayDeque<>();
    quoted.add("'");
    quoted.add("(");
    quoted.add("a");
    quoted.add(")");
    Element elem = Element.parse(quoted);
    assertEquals(quoted.size(), 0);
    assertTrue(elem instanceof SExpression);
    assertEquals(((SExpression)elem).getFirst().toString(), "'");
    assertTrue(((SExpression)elem).getRest() instanceof SExpression);
  }

  @Test
  void parseList() {
    Deque<String> list = new ArrayDeque<>();
    list.add("(");
    list.add("a");
    list.add(")");
    Element elem = Element.parse(list);
    assertEquals(list.size(), 0);
    assertTrue(elem instanceof SExpression);
  }

  @Test
  void parseQuotedPair() {
    Deque<String> list = new ArrayDeque<>();
    list.add("'");
    list.add("(");
    list.add("a");
    list.add(".");
    list.add("b");
    list.add(")");
    Element elem = Element.parse(list);
    assertEquals(list.size(), 0);
    assertTrue(elem instanceof SExpression);
  }
}