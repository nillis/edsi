package com.nillis.edsi.scan;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ScannerTest {
  @Test
  void isBalancedWorksOnEmpty() {
    List<String> empty = new LinkedList<>();
    assertTrue(Scanner.isBalanced(empty));
  }

  @Test
  void isBalancedSimple() {
    List<String> simple = new LinkedList<>();
    simple.add("(");
    assertFalse(Scanner.isBalanced(simple));
    simple.add("a");
    assertFalse(Scanner.isBalanced(simple));
    simple.add("(");
    assertFalse(Scanner.isBalanced(simple));
    simple.add(")");
    assertFalse(Scanner.isBalanced(simple));
    simple.add(")");
    assertTrue(Scanner.isBalanced(simple));
  }

  @Test
  void isBalancedThrowsOnUnbalanced() {
    List<String> simple = new LinkedList<>();
    simple.add("(");
    simple.add(")");
    simple.add(")");
    assertThrows(IllegalArgumentException.class, () -> Scanner.isBalanced(simple));
  }

  @Test
  void processLineEOS() {
    Scanner s = new Scanner("");
    List<String> tokens = s.processLine();
    assertEquals(tokens.size(), 0);
  }

  @Test
  void processLineEmpty() {
    Scanner s = new Scanner("\n");
    List<String> tokens = s.processLine();
    assertEquals(tokens.size(), 0);
  }

  @Test
  void processLineEmptyList() {
    Scanner s = new Scanner("()\n");
    List<String> tokens = s.processLine();
    assertEquals(tokens.size(), 2);
    assertEquals(tokens.get(0), "(");
    assertEquals(tokens.get(1), ")");
  }

  @Test
  void processLineOneAtom() {
    Scanner s = new Scanner("alpha\n");
    List<String> tokens = s.processLine();
    assertEquals(tokens.size(), 1);
    assertEquals(tokens.get(0), "alpha");
  }

  @Test
  void processLineOneQuotedAtom() {
    Scanner s = new Scanner("'alpha\n");
    List<String> tokens = s.processLine();
    assertEquals(tokens.size(), 2);
    assertEquals(tokens.get(0), "'");
    assertEquals(tokens.get(1), "alpha");
  }

  @Test
  void processLineOneQuotedList() {
    Scanner s = new Scanner("'(a)\n");
    List<String> tokens = s.processLine();
    assertEquals(tokens.size(), 4);
    assertEquals(tokens.get(0), "'");
    assertEquals(tokens.get(1), "(");
    assertEquals(tokens.get(2), "a");
    assertEquals(tokens.get(3), ")");
  }

  @Test
  void processLineCarCall() {
    Scanner s = new Scanner("(car '(a b))\n");
    List<String> tokens = s.processLine();
    assertEquals(tokens.size(), 8);
    assertEquals(tokens.get(0), "(");
    assertEquals(tokens.get(1), "car");
    assertEquals(tokens.get(2), "'");
    assertEquals(tokens.get(3), "(");
    assertEquals(tokens.get(4), "a");
    assertEquals(tokens.get(5), "b");
    assertEquals(tokens.get(6), ")");
    assertEquals(tokens.get(7), ")");
  }

  @Test
  void nextCanHandleMultiLines() {
    Scanner s = new Scanner("(car\n\t'(a b)\n)\n");
    List<String> tokens = s.next();
    assertEquals(tokens.size(), 8);
    assertEquals(tokens.get(0), "(");
    assertEquals(tokens.get(1), "car");
    assertEquals(tokens.get(2), "'");
    assertEquals(tokens.get(3), "(");
    assertEquals(tokens.get(4), "a");
    assertEquals(tokens.get(5), "b");
    assertEquals(tokens.get(6), ")");
    assertEquals(tokens.get(7), ")");
  }

  @Test
  void processString() {
    String token = "\"test\"";
    Scanner s = new Scanner(token);
    List<String> tokens = s.next();
    assertEquals(tokens.size(), 1);
    assertEquals(tokens.get(0), token);
  }

  @Test
  void processStringThrows() {
    Scanner s = new Scanner("\"test\nmore test\"\n");
    assertThrows(IllegalArgumentException.class, s::next);
  }
}