(define map--
    (lambda (f lst)
        (cond
            ((pair? lst) (cons (f (car lst)) (map-- f (cdr lst))))
            ((null? lst) '())
            (else (error "Argument to 'map--' not a proper list")))))

(define append--
    (lambda (l m)
        (cond
            ((null? l) m)
            (else (cons (car l) (append-- (cdr l) m))))))

(define length--
    (lambda (l)
        (cond
            ((null? l) 0)
            ((list? l) (+ 1 (length-- (cdr l))))
            (else (error "Argument to 'length--' not a proper list")))))
