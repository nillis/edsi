package com.nillis.edsi.parse;

public class NumericAtom extends Atom {
  private final int value;

  public NumericAtom(String token) {
    super(token);
    value = Integer.parseInt(token);
  }

  public NumericAtom(int value) {
    super(Integer.toString(value));
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public String toString() {
    return Integer.toString(value);
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof NumericAtom && ((NumericAtom)obj).value == value;
  }
}
