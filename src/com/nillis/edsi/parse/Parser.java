package com.nillis.edsi.parse;

import java.util.ArrayDeque;
import java.util.List;

/**
 * A parser responsible for turning a sequence of string tokens into an internal tree representation appropriate for
 * evaluation
 */
public class Parser {
  public static Element parse(List<String> tokens) {
    return Element.parse(new ArrayDeque<>(tokens));
  }
}
