package com.nillis.edsi.parse;

import java.util.Deque;

/**
 * Base data structure for representing Scheme atoms and s-expressions
 */
public abstract class Element {
  public static Element parse(Deque<String> stack) {
    /* TODO
    switch(stack.peek()) {
      case "'":
        Element first = new Atom(stack.pop());
        Element rest = Element.parse(stack);
        return new SExpression(first, rest);
      case "(":
        return new SExpression(stack);
      default:
        String token = stack.pop();
        if (token.charAt(0) == '"') {
          return new StringAtom(token);
        } else if (token.matches("[\\d\\+\\-]?[\\d]+")) {
          return new NumericAtom(token);
        } else {
          return new Atom(token);
        }
    }
     */
    return null;
  }
}
