package com.nillis.edsi.parse;

/**
 * Class representing a Scheme atom
 */
public class Atom extends Element {
  public static final Atom TRUE = new Atom("#t");
  public static final Atom FALSE = new Atom("#f");

  private final String token;

  public Atom(String token) {
    this.token = token;
  }

  public String toString() {
    return token;
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof Atom && ((Atom) obj).token.equalsIgnoreCase(token);
  }
}
