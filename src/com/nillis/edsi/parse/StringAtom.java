package com.nillis.edsi.parse;

public class StringAtom extends Atom {
  public StringAtom(String token) {
    super(token);
  }

  public String getContent() {
    String token = toString();
    return token.substring(1, token.length() - 1);
  }
}
