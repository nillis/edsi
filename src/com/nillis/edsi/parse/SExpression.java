package com.nillis.edsi.parse;

import java.util.Deque;

/**
 * Class representing a Scheme s-expression
 */
public class SExpression extends Element {
  public static final SExpression EMPTY_LIST = new SExpression(null, null);
  private Element first;
  private Element rest;

  public SExpression(Element first, Element rest) {
    this.first = first;
    this.rest = rest;
  }

  public SExpression(Deque<String> stack) {
    // TODO: build an s-expression from the stack of token
  }

  public Element getFirst() {
    return first;
  }

  public void setFirst(Element first) {
    this.first = first;
  }

  public Element getRest() {
    return rest;
  }

  public void setRest(Element rest) {
    this.rest = rest;
  }

  /**
   * Append an element to the current s-expression.  The element goes in the first available "slot"
   * @param elem the element to add
   */
  public void append(Element elem) {
    if (first == null) {
      first = elem;
    } else if (rest == null) {
      rest = new SExpression(elem, null);
    } else if (rest instanceof SExpression){
      ((SExpression)rest).append(elem);
    } else {
      throw new IllegalArgumentException("Cannot add element to improper list.");
    }
  }

  public int length() {
    if (first == null) {
      return 0;
    } else if (rest == null) {
      return 1;
    } else if (rest instanceof SExpression) {
      return 1 + ((SExpression)rest).length();
    } else {
      return -1; // improper list
    }
  }

  public SExpression shallowCopy() {
    Element rest = getRest();
    if (rest != null && rest instanceof SExpression) {
      rest = ((SExpression)rest).shallowCopy();
    }
    return new SExpression(getFirst(), rest);
  }

  public SExpression lastPair() {
    SExpression crt = this;
    while (crt.getRest() != null && crt.getRest() instanceof SExpression) {
      crt = (SExpression)crt.getRest();
    }
    return crt;
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    // TODO: return a pretty string version of the s-expression
    return sb.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof SExpression) || obj == null) {
      return false;
    }
    SExpression sobj = (SExpression)obj;
    if (first != sobj.first) {
      if (first == null || sobj.first == null || !first.equals(sobj.first)) {
        return false;
      }
    }
    if (rest != sobj.rest) {
      if (rest == null || sobj.rest == null || !rest.equals(sobj.rest)) {
        return false;
      }
    }
    return true;
  }
}
