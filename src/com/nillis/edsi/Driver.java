package com.nillis.edsi;

import com.nillis.edsi.parse.Element;
import com.nillis.edsi.parse.Parser;
import com.nillis.edsi.scan.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStreamReader;
import java.util.List;

public class Driver {
  private static final Logger log = LogManager.getLogger(Driver.class);

  public Driver() {}

  void initialize(String resource) {
    log.info("Initializing...");
    Scanner scanner = new Scanner(new InputStreamReader(Driver.class.getResourceAsStream(resource)));
    while(true) {
      List<String> tokens = scanner.next();
      if (tokens.size() == 0) {
        break;
      } else {
        try {
          Element elem = Parser.parse(tokens);
          // TODO: evaluate the parsed input
        } catch(IllegalArgumentException e) {
          System.err.println(e.getMessage());
        }
      }
    }
    scanner.close();
  }

  private void repl() {
    // enter read, evaluate, print loop. Exit loop when 'quit' command is given
    Scanner scanner = new Scanner(new InputStreamReader(System.in));
    while (true) {
      try {
        List<String> tokens = scanner.next();
        if (tokens.size() == 0) {
          break;
        } else if (tokens.get(0).equalsIgnoreCase("quit")) {
          break;
        } else {
          Element elem = Parser.parse(tokens);
          log.debug("Parse: " + elem.toString());
          // TODO: evaluate the parsed input
        }
      } catch(IllegalArgumentException e) {
        System.err.println(e.getMessage());
      }
    }
    log.info("Shutting down...");
  }

  public static void main(String[] args) {
    log.info("Starting...");
    Driver driver = new Driver();
    driver.initialize("/schemer.scm");
    driver.repl();
  }
}
