package com.nillis.edsi.scan;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;

/**
 * A simple lexer that can turn an input string or reader into a sequence of tokens, following Scheme/List
 * syntax
 */
public class Scanner {
  private static final Logger log = LogManager.getLogger(Scanner.class);
  private static final String SINGLE_TOKENS = "()'.";
  private static final String WHITESPACE = " \t\n\r";
  private static final String NUMERIC_TOKENS = "+-0123456789";
  private static final String NUMERIC_TOKENS_BODY = "0123456789";
  private static final String LITERAL_TOKENS = "_#*<>=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  private static final String LITERAL_TOKENS_BODY = "-=?_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

  private final BufferedReader input;

  public Scanner(String input) { this.input = new BufferedReader((new StringReader(input))); }

  public Scanner(Reader input) {
    this.input = new BufferedReader(input);
  }

  public void close() {
    try {
      input.close();
    } catch(Exception e) {
      log.error(e, e);
    }
  }

  public List<String> next() {
    List<String> tokens = new LinkedList<>();
    while(tokens.size() == 0 || !isBalanced(tokens)) {
      List<String> newTokens = processLine();
      if(newTokens != null) {
        tokens.addAll(newTokens);
      } else {
        break;
      }
    }
    return tokens;
  }

  static public boolean isBalanced(List<String> tokens) {
    int count = 0;
    for (String token : tokens) {
      if(token.equals("(")) {
        count++;
      }
      else if(token.equals(")")) {
        count--;
        if (count < 0) {
          throw new IllegalArgumentException("Expression not balanced.");
        }
      }
    }
    return count == 0;
  }

  List<String> processLine() {
    List<String> tokens = new LinkedList<>();
    try {
      String line = input.readLine();
      if(line == null || line.length() == 0) {
        return tokens;
      }
      int index = 0;
      while (index < line.length()) {
        char c = line.charAt(index);
        if (SINGLE_TOKENS.indexOf(c) >= 0) {
          tokens.add(new StringBuilder().append(c).toString());
          index++;
        } else if (WHITESPACE.indexOf(c) >= 0) {
          index++;
        } else if (NUMERIC_TOKENS.indexOf(c) >= 0) {
          String token = consume(line, index, NUMERIC_TOKENS_BODY);
          tokens.add(token);
          index += token.length();
        } else if (LITERAL_TOKENS.indexOf(c) >= 0) {
          String token = consume(line, index, LITERAL_TOKENS_BODY);
          tokens.add(token);
          index += token.length();
        } else if (c == '"') {
          String token = consumeString(line, index);
          tokens.add(token);
          index += token.length();
        } else {
          throw new IllegalArgumentException("Invalid token");
        }
      }
      return tokens;
    } catch (IOException ie) {
      log.error(ie, ie);
      return tokens;
    }
  }

  // Read a sequence of characters from the 'line' string, starting after position 'i'; only allow characters
  // from the given 'characters' list
  // Return the resulting token
  private String consume(String line, int i, String characters) {
    int j = i + 1;
    while (j < line.length() && characters.indexOf(line.charAt(j)) >= 0) {
      j++;
    }
    return line.substring(i, j);
  }

  // Read a string token, delimited by double quotes, from the 'line' string, starting after position 'i'
  // Return the resulting token
  private String consumeString(String line, int i) {
    int j = i + 1;
    while (j < line.length() && line.charAt(j) != '"') {
      j++;
    }
    if (j == line.length()) {
      throw new IllegalArgumentException("Non-terminated string.");
    }
    return line.substring(i, j + 1);
  }
}
